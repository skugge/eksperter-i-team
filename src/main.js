import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import firebase from "firebase";
Vue.config.productionTip = false;

let app = "";
// Initialize Firebase
const config = {
  apiKey: "AIzaSyDxbhspsoM7XO8LO9b-tDbIGXr-Ok8ovkg",
  authDomain: "eksperter-i-team.firebaseapp.com",
  databaseURL: "https://eksperter-i-team.firebaseio.com",
  projectId: "eksperter-i-team",
  storageBucket: "eksperter-i-team.appspot.com",
  messagingSenderId: "532537861134"
};
firebase.initializeApp(config);

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount("#app");
  }
});
