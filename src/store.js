import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    sidebar: null
  },
  mutations: {
    toggleNavbar(state) {
      state.sidebar = !state.sidebar;
    }
  },
  actions: {}
});
