import Vue from "vue";
import Vuetify from "vuetify/lib";
import "vuetify/src/stylus/app.styl";

Vue.use(Vuetify, {
  iconfont: "md",
  theme: {
    primary: "#3ba170",
    secondary: "#878FA9",
    accent: "#82B1FF",
    error: "#FF5252",
    info: "#2196F3",
    success: "#4CAF50",
    warning: "#FFC107",
    background_white: "#0076cf",
    background_dark: "#1e1f21",
    component_light: "#fff",
    component_dark: "#424242"
  }
});
