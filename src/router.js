import Vue from "vue";
import Router from "vue-router";
import Home2 from "./views/Home2.vue";
import Raw from "./views/Raw.vue";
import Map from "./views/Map.vue";
import Login from "./views/Login.vue";
import SignUp from "./views/SignUp.vue";
import firebase from "firebase";
import Mock from "./views/Mock.vue";
import Analytics from "./views/Analytics.vue";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "*",
      redirect: "/"
    },
    {
      path: "/",
      redirect: "/home"
    },
    {
      path: "/home",
      name: "home",
      component: Home2,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    },
    /*
    {
      path: "/signup",
      name: "SignUp",
      component: SignUp
    }, 
    */
    {
      path: "/raw",
      name: "raw",
      component: Raw,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/map",
      name: "map",
      component: Map,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/mock",
      name: "mock",
      component: Mock,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/analytics",
      name: "analytics",
      component: Analytics,
      meta: {
        requiresAuth: false
      }
    }
  ]
});

/* router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiresAuth && !currentUser) next("login");
  else if (!requiresAuth && currentUser) next("home");
  else next();
}); */

export default router;
